package com.virgotest.tmdb.utils.zeedata.exception

class FailureRemoteResultException(message: String) : Exception(message)