package com.virgotest.tmdb.utils.zeecommon.util.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.virgotest.tmdb.utils.zeecommon.util.CombinedTransformation

class AppDataWrapperWithFilter<T, E>(
    private val blockFilter: (filterEntity: E?, dataList: List<T>) -> List<T>
) {

    val filter by lazy { MutableLiveData<E>() }

    val sourceLiveData by lazy { MutableLiveData(listOf<T>()) }
    val liveData by lazy {
        CombinedTransformation(filter, sourceLiveData) {
            val filterEntity = (it[0] as? E)
            val sourceList = it[1] as? List<T>
            return@CombinedTransformation blockFilter(filterEntity, sourceList.orEmpty())
        }
    }

    val isEmpty by lazy { Transformations.map(liveData) { it.isEmpty() } }
    val isLoading by lazy { MutableLiveData(false) }

    fun setFilter(filterEntity: E) {
        filter.value = filterEntity
    }
}