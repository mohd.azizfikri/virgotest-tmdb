package com.virgotest.tmdb.utils.zeewidget

import android.content.Context
import android.graphics.PorterDuff
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import com.virgotest.tmdb.R

/**
 * Author:
 * Aziz Fikri 2021
 */
class LargeToolbar(
    context: Context,
    attrs: AttributeSet?
) : FrameLayout(context, attrs) {

    private val viewRoot: View = inflate(context, R.layout.zeewidget_large_toolbar, this)
    private val ltoolbarContainer: ViewGroup = findViewById(R.id.ltoolbar_container)
    private val ivIcon1: ImageView = findViewById(R.id.icon1)
    private val ivIcon2: ImageView = findViewById(R.id.icon2)
    private val ivIcon3: ImageView = findViewById(R.id.icon3)
    private val moreMargin: View = findViewById(R.id.additionMargin)
    private val tvText1: TextView = findViewById(R.id.text1)

    init {
        val typedAttrs = context.obtainStyledAttributes(attrs, R.styleable.LargeToolbar)

        setTitle(typedAttrs.getString(R.styleable.LargeToolbar_title))

        val alignment = when (typedAttrs.getString(R.styleable.LargeToolbar_titleAlignment)) {
            "0" -> Alignment.TITLE_START
            "1" -> Alignment.TITLE_CENTER
            "2" -> Alignment.TITLE_END
            else -> Alignment.TITLE_START
        }
        setTitleAlignment(alignment)

        if (typedAttrs.getBoolean(R.styleable.LargeToolbar_iconPrimary_enabled, true)) showPrimaryIcon() else hidePrimaryIcon()
        if (typedAttrs.getBoolean(R.styleable.LargeToolbar_iconSecondary_enabled, true)) showSecondaryIcon() else hideSecondaryIcon()
        if (typedAttrs.getBoolean(R.styleable.LargeToolbar_iconThird_enabled, false)) showThirdIcon() else hideThirdIcon()
        if (typedAttrs.getBoolean(R.styleable.LargeToolbar_setTitleAbsolutely_center, false)) absolutelyCenter(true) else absolutelyCenter(false)

        setPrimaryIconImage(
            typedAttrs.getResourceId(
                R.styleable.LargeToolbar_iconPrimary_src,
                R.drawable.ic_arrow_back_white
            )
        )

        setSecondaryIconImage(
            typedAttrs.getResourceId(
                R.styleable.LargeToolbar_iconSecondary_src,
                R.drawable.ic_cart
            )
        )
        setThirdIconImage(
            typedAttrs.getResourceId(
                R.styleable.LargeToolbar_iconThird_src,
                R.drawable.ic_search
            )
        )

        if (typedAttrs.getBoolean(R.styleable.LargeToolbar_inverse, false)) {
            setInverseBackground(true)
        }

        if (typedAttrs.getBoolean(R.styleable.LargeToolbar_iconPrimary_inverse, false)) {
            setInverseIconPrimary(true)
        }

        if (typedAttrs.getBoolean(R.styleable.LargeToolbar_iconSecondary_inverse, false)) {
            setInverseIconSecondary(true)
        }

        if (typedAttrs.getBoolean(R.styleable.LargeToolbar_iconThird_inverse, false)) {
            setInverseIconThird(true)
        }

        typedAttrs.recycle()
    }

    fun setOnPrimaryIconClick(fn: (v: View) -> Unit) {
        ivIcon1.setOnClickListener { fn(it) }
    }

    fun setPrimaryIconImage(@DrawableRes image: Int) {
        ivIcon1.setImageResource(image)
    }

    fun showPrimaryIcon() {
        if (ivIcon1.visibility == View.VISIBLE) return
        ivIcon1.visibility = View.VISIBLE
    }

    fun hidePrimaryIcon() {
        if (ivIcon1.visibility == View.GONE) return
        ivIcon1.visibility = View.GONE
    }

    fun setOnSecondaryIconClick(fn: (v: View) -> Unit) {
        ivIcon2.setOnClickListener { fn(it) }
    }

    fun showSecondaryIcon() {
        if (ivIcon2.visibility == View.VISIBLE) return
        ivIcon2.visibility = View.VISIBLE
    }

    fun hideSecondaryIcon() {
        if (ivIcon2.visibility == View.GONE) return
        ivIcon2.visibility = View.GONE
    }

    fun showThirdIcon() {
        if (ivIcon3.visibility == View.VISIBLE) return
        ivIcon3.visibility = View.VISIBLE
    }

    fun hideThirdIcon() {
        if (ivIcon3.visibility == View.GONE) return
        ivIcon3.visibility = View.GONE
    }

    fun absolutelyCenter(isit: Boolean) {
        when (isit){
            true -> {
                if (moreMargin.visibility == View.VISIBLE) return
                else moreMargin.visibility = View.VISIBLE
            }
            else -> {
                if (moreMargin.visibility == View.GONE) return
                else moreMargin.visibility = View.GONE
            }
        }
    }

    fun setOnThirdIconClick(fn: (v: View) -> Unit) {
        ivIcon3.setOnClickListener { fn(it) }
    }

    fun setSecondaryIconImage(@DrawableRes image: Int) {
        ivIcon2.setImageResource(image)
    }

    fun setThirdIconImage(@DrawableRes image: Int) {
        ivIcon3.setImageResource(image)
    }

    fun setTitle(text: String?) {
        tvText1.text = text
    }

    fun setTitleAlignment(alignment: Alignment) {
        tvText1.gravity = when (alignment) {
            Alignment.TITLE_START -> Gravity.START
            Alignment.TITLE_CENTER -> Gravity.CENTER
            Alignment.TITLE_END -> Gravity.END
        }
    }

    fun setInverseBackground(inverse: Boolean) {
        // background
        val baseColorRes = if (inverse) R.color.toolbar_colorInverse else R.color.toolbar_colorPrimary
        val baseColor = ResourcesCompat.getColor(resources, baseColorRes, null)
        ltoolbarContainer.setBackgroundColor(baseColor)

        // text
        val textColorRes = if (inverse) R.color.toolbar_colorPrimary else R.color.toolbar_colorInverse
        val textColor = ResourcesCompat.getColor(resources, textColorRes, null)
        tvText1.setTextColor(textColor)
    }

    fun setInverseIconPrimary(inverse: Boolean) {
        val iconColorRes = if (inverse) R.color.toolbar_colorPrimary else {
            ivIcon1.clearColorFilter()
            return
        }

        val iconColor = ResourcesCompat.getColor(resources, iconColorRes, null)
        ivIcon1.setColorFilter(iconColor, PorterDuff.Mode.MULTIPLY)
    }

    fun setInverseIconSecondary(inverse: Boolean) {
        val iconColorRes = if (inverse) R.color.toolbar_colorPrimary else {
            ivIcon2.clearColorFilter()
            return
        }

        val iconColor = ResourcesCompat.getColor(resources, iconColorRes, null)
        ivIcon2.setColorFilter(iconColor, PorterDuff.Mode.MULTIPLY)
    }


    fun setInverseIconThird(inverse: Boolean) {
        val iconColorRes = if (inverse) R.color.toolbar_colorPrimary else {
            ivIcon3.clearColorFilter()
            return
        }

        val iconColor = ResourcesCompat.getColor(resources, iconColorRes, null)
        ivIcon3.setColorFilter(iconColor, PorterDuff.Mode.MULTIPLY)
    }

    enum class Alignment { TITLE_START, TITLE_CENTER, TITLE_END }

}