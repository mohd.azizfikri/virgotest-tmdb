package com.virgotest.tmdb.utils.zeedata.exception

class FailureDbResultException(message: String) : Exception(message)