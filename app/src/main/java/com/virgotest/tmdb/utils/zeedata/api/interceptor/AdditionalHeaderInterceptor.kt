package com.virgotest.tmdb.utils.zeedata.api.interceptor

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

abstract class AdditionalHeaderInterceptor : Interceptor {

    abstract fun Request.Builder.onRequestBuilding()

    override fun intercept(chain: Interceptor.Chain): Response {
        val interceptedRequest = chain
            .request()
            .newBuilder()
            .apply { onRequestBuilding() }
            .build()

        return chain.proceed(interceptedRequest)
    }

}