package com.virgotest.tmdb.utils.zeecommon.binding

import android.os.Build
import android.view.View
import android.widget.*
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.load
import coil.transform.CircleCropTransformation
import com.virgotest.tmdb.R
import java.io.File

/**
 * View ==================================================================================
 */

@BindingAdapter("visible")
fun View.visible(visible: Boolean) {
    val newVisibility = if (visible) View.VISIBLE else View.GONE
    if (newVisibility == visibility) return
    visibility = newVisibility
}

@BindingAdapter("shown")
fun View.shown(visible: Boolean) {
    val newVisibility = if (visible) View.VISIBLE else View.INVISIBLE
    if (newVisibility == visibility) return
    visibility = newVisibility
}

/**
 * RecyclerView ===================================================================================
 */

@BindingAdapter("items")
fun <T> RecyclerView.items(items: List<T>?) {
    if (adapter != null && adapter is ListAdapter<*, RecyclerView.ViewHolder>) {
        (adapter as ListAdapter<T, RecyclerView.ViewHolder>).submitList(items)
    }
}

@BindingAdapter("pagedListItems")
fun <T> RecyclerView.pagedListItems(items: PagedList<T>?) {
    if (adapter != null && adapter is PagedListAdapter<*, RecyclerView.ViewHolder>) {
        (adapter as PagedListAdapter<T, RecyclerView.ViewHolder>).submitList(items) {
            val layoutManager = (layoutManager as LinearLayoutManager)
            val position = layoutManager.findFirstCompletelyVisibleItemPosition()
            if (position != RecyclerView.NO_POSITION) scrollToPosition(position)
        }
    }
}

/**
 * SwipeRefreshLayout ==================================================================================
 */

@BindingAdapter("onRefresh")
fun SwipeRefreshLayout.swipeRefresh(callback: Runnable) {
    setOnRefreshListener {
        callback.run()
    }
}

@BindingAdapter("onShortRefresh")
fun SwipeRefreshLayout.immediateRefresh(callback: Runnable) {
    setOnRefreshListener {
        isRefreshing = false
        callback.run()
    }
}

@BindingAdapter(value = ["loading", "loadingAttrChanged"], requireAll = false)
fun SwipeRefreshLayout.loadingState(
    isLoading: Boolean,
    loadingAttrChanged: InverseBindingListener
) {
    if (isRefreshing != isLoading) {
        isRefreshing = isLoading
        loadingAttrChanged.onChange()
    }
}

@InverseBindingAdapter(attribute = "loading", event = "loadingAttrChanged")
fun SwipeRefreshLayout.onLoadingStateChange(): Boolean = isRefreshing

/**
 * Spinner ==================================================================================
 */

@BindingAdapter("entries")
fun Spinner.entries(values: List<String>?) {
    values?.let {
        adapter = ArrayAdapter(
            context,
            android.R.layout.simple_spinner_dropdown_item,
            android.R.id.text1,
            values
        )
    }
}

@BindingAdapter(value = ["selectedValue", "selectedValueAttrChanged"], requireAll = false)
fun Spinner.selectedValue(
    newSelectedValue: String?,
    newTextAttrChanged: InverseBindingListener
) {
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            newTextAttrChanged.onChange()
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }
    if (adapter != null && newSelectedValue != null) {
        val pos = (adapter as ArrayAdapter<String>).getPosition(newSelectedValue)
        setSelection(pos, true)
    }
}

/**
 * Spinner ==================================================================================
 */

@InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
fun Spinner.selectedValueAttrChanged(): String {
    return this.selectedItem as String
}

@BindingAdapter("onEditorAction")
fun EditText.onImeAction(callback: Runnable) {
    this.setOnEditorActionListener { view, actionId, event ->
        callback.run()
        true
    }
}

/**
 * ImageView ==================================================================================
 */

@BindingAdapter("loadUrl")
fun ImageView.loadUrl(url: String? = null) {
    if (url == null) return
    load(url) {
        placeholder(R.drawable.common_img_placeholder)
        error(R.drawable.common_img_placeholder)
    }
}

@BindingAdapter("loadFile")
fun ImageView.loadFile(file: File? = null) {
    if (file == null) return
    load(file) {
        placeholder(R.drawable.common_img_placeholder)
        error(R.drawable.common_img_placeholder)
    }
}

@BindingAdapter("loadUrl_animatedCircle")
fun ImageView.loadAnimatedCircleUrl(url: String? = null) {
    if (url == null) return
    load(url) {
        if (Build.VERSION.SDK_INT >= 28) decoder(ImageDecoderDecoder())
        else decoder(GifDecoder())

        placeholder(R.drawable.ic_anim_loading)
        error(R.drawable.ic_placeholder)
        transformations(CircleCropTransformation())
    }
}

@BindingAdapter("loadFile_animatedCircle")
fun ImageView.loadAnimatedCircleFile(url: File? = null) {
    if (url == null) return
    load(url) {
        if (Build.VERSION.SDK_INT >= 28) decoder(ImageDecoderDecoder())
        else decoder(GifDecoder())

        placeholder(R.drawable.ic_anim_loading)
        error(R.drawable.ic_placeholder)
        transformations(CircleCropTransformation())
    }
}
