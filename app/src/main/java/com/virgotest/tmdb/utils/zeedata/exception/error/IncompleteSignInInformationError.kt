package com.virgotest.tmdb.utils.zeedata.exception.error

class IncompleteSignInInformationError(message: String? = null) : Exception(message)