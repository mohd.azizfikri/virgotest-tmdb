package com.virgotest.tmdb.utils.zeedata.api.response.general

import com.google.gson.annotations.SerializedName

/**
 * Data of object
 */
data class MessageResponse(
    @field:SerializedName("status", alternate = ["status_code"]) val status: Int = 0,
    @field:SerializedName("message", alternate = ["status_message"]) val message: String = "",
    @field:SerializedName("success") val success: Boolean = false
)