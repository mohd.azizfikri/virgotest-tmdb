package com.virgotest.tmdb.utils.zeedata.repository

import com.natpryce.*
import com.virgotest.tmdb.utils.zeedata.api.response.auth.NewSessionResponse
import com.virgotest.tmdb.utils.zeedata.api.response.auth.TokenResponse
import com.virgotest.tmdb.utils.zeedata.db.entity.Configuration
import com.virgotest.tmdb.utils.zeedata.util.AppRepository

class CommonRepository : AppRepository()  {

    suspend fun createGuestSession(): Result<NewSessionResponse, Exception> {
        val result = asDirectResult { getApi().createGuestSession() }.onFailure { return it }

        result.let { data ->
            // save session id immediately
            val config = Configuration(Configuration.KEY_SESSION, data.guest_session_id)

            getAppDb()
                .configurationDao()
                .insert(config)
        }

        return Success(result)
    }

    fun getSavedSessionID(): Result<String, Exception> {
        val mSessionID = getAppDb().configurationDao().getOneFromMainThread(Configuration.KEY_SESSION)
        return mSessionID?.let {
            Success(it.content)
        } ?: Failure(Exception("No Saved Session ID"))
    }

    suspend fun createRequestToken(): Result<TokenResponse, Exception> {
        val result = asDirectResult { getApi().createRequestToken() }.onFailure { return it }
        return Success(result)
    }

    suspend fun deleteSession(session: String): Result<NewSessionResponse, Exception> {
        val result = asDirectResult { getApi().deleteSession(session) }.onFailure { return it }
        return Success(result)
    }


}