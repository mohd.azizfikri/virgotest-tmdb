package com.virgotest.tmdb.utils.zeedata.api

import com.virgotest.tmdb.utils.zeedata.api.response.auth.NewSessionResponse
import com.virgotest.tmdb.utils.zeedata.api.response.auth.TokenResponse
import com.virgotest.tmdb.utils.zeedata.api.response.general.BasicPagesResponse
import com.virgotest.tmdb.utils.zeedata.api.response.content.ContentResponse
import com.virgotest.tmdb.utils.zeedata.api.response.content.reviews.ReviewResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    /**
     * AUTHENTICATION ==============================================================================
     */

    @GET("/3/authentication/guest_session/new")
    suspend fun createGuestSession(
        @Query("api_key") api_key: String = "9ae74191c0b6db7d9d92dd33c3594241"
    ): Response<NewSessionResponse>

    @GET("/3/authentication/token/new")
    suspend fun createRequestToken(
        @Query("api_key") api_key: String = "9ae74191c0b6db7d9d92dd33c3594241"
    ): Response<TokenResponse>

    @FormUrlEncoded
    @DELETE("/3/authentication/guest_session/new")
    suspend fun deleteSession(
        @Field("session_id") session: String,
        @Query("api_key") api_key: String = "9ae74191c0b6db7d9d92dd33c3594241"
    ): Response<NewSessionResponse>


    /**
     * LIST CONTENT ================================================================================
     */

    @GET("/3/trending/{media_type}/{time_window}")
    suspend fun getTrending(
        @Path("media_type") media_type: String = "movie",
        @Path("time_window") time_window: String = "day",
        @Query("api_key") api_key: String = "9ae74191c0b6db7d9d92dd33c3594241"
    ): Response<BasicPagesResponse<ContentResponse>>

    @GET("/3/discover/movie")
    suspend fun getDiscoverMovie(
        @Query("api_key") api_key: String = "9ae74191c0b6db7d9d92dd33c3594241",
        @Query("page") page: String,
        @Query("language") language: String,
        @Query("region") region: String,
        @Query("sort_by") sort_by: String,
        @Query("certification_country") certification_country: String,
        @Query("certification") certification: String,
        @Query("certification.lte") certificationLte: String,
        @Query("certification.gte") certificationGte: String,
        @Query("include_adult") include_adult: Boolean,
        @Query("include_video") include_video: Boolean,
        @Query("year") year: Int? = null,
    ): Response<BasicPagesResponse<ContentResponse>>

    @GET("/3/discover/tv")
    suspend fun getDiscoverTV(
        @Query("api_key") api_key: String = "9ae74191c0b6db7d9d92dd33c3594241",
        @Query("page") page: String,
        @Query("language") language: String,
        @Query("region") region: String,
        @Query("sort_by") sort_by: String,
        @Query("certification_country") certification_country: String,
        @Query("certification") certification: String,
        @Query("certification.lte") certificationLte: String,
        @Query("certification.gte") certificationGte: String,
        @Query("include_adult") include_adult: Boolean,
        @Query("include_video") include_video: Boolean,
        @Query("year") year: Int? = null,
    ): Response<BasicPagesResponse<ContentResponse>>

    @GET("/3/{media_type}/{content_id}")
    suspend fun getDetailContent(
        @Path("content_id") content_id: String,
        @Path("media_type") media_type: String = "movie",
        @Query("language") language: String = "en-US",
        @Query("api_key") api_key: String = "9ae74191c0b6db7d9d92dd33c3594241"
    ): Response<ContentResponse>

    @GET("/3/{media_type}/{content_id}/reviews")
    suspend fun getContentReview(
        @Path("content_id") content_id: String,
        @Path("media_type") media_type: String = "movie",
        @Query("page") page: String = "1",
        @Query("language") language: String = "en-US",
        @Query("api_key") api_key: String = "9ae74191c0b6db7d9d92dd33c3594241"
    ): Response<BasicPagesResponse<ReviewResponse>>


}