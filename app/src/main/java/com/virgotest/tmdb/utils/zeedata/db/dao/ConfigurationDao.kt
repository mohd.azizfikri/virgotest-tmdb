package com.virgotest.tmdb.utils.zeedata.db.dao

import androidx.room.*
import com.virgotest.tmdb.utils.zeedata.db.entity.Configuration

@Dao
interface ConfigurationDao {

    @Query("SELECT * FROM Configuration")
    suspend fun getConfigure(): Configuration?

    @Query("SELECT * FROM Configuration WHERE identifier == :key")
    fun getOneFromMainThread(key: String): Configuration?

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(vararg config: Configuration)

    // if there is new config, replace the old one.
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg config: Configuration)

    @Query("DELETE FROM Configuration WHERE identifier = :key")
    suspend fun remove(key: String)

    @Query("DELETE FROM Configuration")
    suspend fun clear()

    @Query("DELETE FROM Configuration")
    fun clearOnMainThread()

}