package com.virgotest.tmdb.utils.zeecommon.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.virgotest.tmdb.utils.zeecommon.util.event.LiveEvent
import com.virgotest.tmdb.utils.zeecommon.util.event.MutableLiveEvent
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

abstract class AppViewModel : ViewModel() {

    // base page loading, it  may be used for entire page loading.
    val loading by lazy { MutableLiveData(false) }

    protected val _showToastMessageEvent by lazy { MutableLiveEvent<String>() }
    val showToastMessageEvent: LiveEvent<String> = _showToastMessageEvent

    protected val _showConfirmationMessageEvent by lazy { MutableLiveEvent<Pair<String, () -> Unit>>() }
    val showConfirmationMessageEvent: LiveEvent<Pair<String, () -> Unit>> = _showConfirmationMessageEvent

    protected val _showAlertMessageEvent by lazy { MutableLiveEvent<Pair<String, () -> Unit>>() }
    val showAlertMessageEvent: LiveEvent<Pair<String, () -> Unit>> = _showAlertMessageEvent

    protected val mCachedJob: MutableMap<Int, Job> = mutableMapOf()

    protected var mDefaultConfirmationMessage = "Are you sure?"

    protected fun launchScope(id: Int, block: suspend () -> Unit) {
        if (mCachedJob.containsKey(id)) {
            if (mCachedJob[id]?.isActive == true) {
                mCachedJob[id]?.cancel()
            }
        }
        mCachedJob[id] = viewModelScope.launch { block() }
    }

    protected fun showToast(message: String?) {
        _showToastMessageEvent.set(message.orEmpty())
    }

    // TODO use custom dialog or else, and use confirmation button
    protected fun showMessage(message: String?, nextAction: () -> Unit = {}) {
        showToast(message)
        nextAction()
    }

    // TODO use custom dialog or else, and use confirmation button
    protected fun showFailureMessage(message: String?, nextAction: () -> Unit = {}) {
        showToast(message)
        nextAction()
    }

    protected fun showFailureMessage(exception: Exception?, nextAction: () -> Unit = {}) {
        showToast(exception?.message)
        nextAction()
    }

    protected fun showConfirmationMessage(message: String?, onConfirmed: () -> Unit = {}) {
        _showConfirmationMessageEvent.set(Pair(message ?: mDefaultConfirmationMessage, onConfirmed))
    }

    protected fun showAlertMessage(message: String, onConfirmed: () -> Unit = {}) {
        _showAlertMessageEvent.set(Pair(message, onConfirmed))
    }

}