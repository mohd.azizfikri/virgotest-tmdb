package com.virgotest.tmdb.utils.zeedata

import com.virgotest.tmdb.utils.zeedata.db.AppDatabase
import com.virgotest.tmdb.utils.zeedata.prefs.AppPrefs

data class DataConfiguration(
    val remoteHost: String = "",
    val remoteAPIkey: String = "",
    val remoteTimeout: Long = 30,
    val databaseName: String = AppDatabase.DEFAULT_DATABASE_NAME,
    val prefsName: String = AppPrefs.DEFAULT_PREFS_NAME,
    val prefsAlwaysCommit: Boolean = false,
    val debug: Boolean = true
)