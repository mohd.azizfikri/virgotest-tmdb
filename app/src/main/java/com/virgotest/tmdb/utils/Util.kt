package com.virgotest.tmdb.utils

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.Spanned
import com.virgotest.tmdb.utils.zeecommon.extension.toMoneyFormat
import java.text.SimpleDateFormat
import java.util.*

object Util {

    private var mContext: Context? = null

    fun init(context: Context) {
        mContext = context
    }

    @JvmStatic
    fun implementHtml(bsa: String): Spanned{
        return  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(bsa, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(bsa)
        }
    }

    @JvmStatic
    fun changeDateFormat(source: String?, oldFormat: String): String{
        return if(source.isNullOrEmpty()){
            ""
        }else {
            val sdf = SimpleDateFormat(oldFormat)
            val d: Date = sdf.parse(source)
            sdf.applyPattern("DD MMM YYYY")
            sdf.format(d)
        }

    }

}