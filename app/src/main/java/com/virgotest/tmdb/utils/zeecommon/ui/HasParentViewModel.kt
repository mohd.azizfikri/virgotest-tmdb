package com.virgotest.tmdb.utils.zeecommon.ui

interface HasParentViewModel {

    val parentViewModel: AppViewModel

}