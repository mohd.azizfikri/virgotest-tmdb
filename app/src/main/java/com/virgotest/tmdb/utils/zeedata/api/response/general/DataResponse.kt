package com.virgotest.tmdb.utils.zeedata.api.response.general

import com.google.gson.annotations.SerializedName

/**
 * Data of object
 */
data class DataResponse<T>(
//    @field:SerializedName("status") val status: Int = 0,
    @field:SerializedName("message", alternate = ["messages"]) val message: String = "",
    @field:SerializedName("data", alternate = ["poin"]) val data: T? = null
)