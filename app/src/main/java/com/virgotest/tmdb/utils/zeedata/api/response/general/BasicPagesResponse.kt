package com.virgotest.tmdb.utils.zeedata.api.response.general

import com.google.gson.annotations.SerializedName

data class BasicPagesResponse<Data>(
    @field:SerializedName("page") val page: Int = 0,
    @field:SerializedName("results") val results: List<Data> = emptyList(),
    @field:SerializedName("total_pages") val totalPages: Int = 0,
    @field:SerializedName("total_results") val totalResults: Int = 0
)