package com.virgotest.tmdb.utils.zeedata.exception

import com.virgotest.tmdb.utils.zeedata.api.response.general.MessageResponse

class UnexpectedHttpResultException(errorResponse: MessageResponse) :
    Exception(errorResponse.message ?: "Error Code ${errorResponse.status}")