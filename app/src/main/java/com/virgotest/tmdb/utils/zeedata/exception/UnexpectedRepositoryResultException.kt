package com.virgotest.tmdb.utils.zeedata.exception

class UnexpectedRepositoryResultException(message: String = "Unexpected error occured."): Exception(message)