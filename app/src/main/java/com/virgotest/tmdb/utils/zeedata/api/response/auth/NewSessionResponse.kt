package com.virgotest.tmdb.utils.zeedata.api.response.auth

data class NewSessionResponse(
    val expires_at: String,
    val guest_session_id: String,
    val success: Boolean
)