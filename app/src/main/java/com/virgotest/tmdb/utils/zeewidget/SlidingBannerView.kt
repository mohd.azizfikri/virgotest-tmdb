package com.virgotest.tmdb.utils.zeewidget

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.virgotest.tmdb.R
import com.virgotest.tmdb.utils.zeewidget.adapter.SlidingBannerAdapter

/**
 * Author:
 * Aditya Chandra
 *
 * https://github.com/addeeandra
 *
 * ===========================================================
 *
 * Call slidingBannerView.init(adapter, mediatorCall?) to initiate.
 * Call slidingBannerView.setItems(List<Item>) to refresh items.
 */
class SlidingBannerView(
    context: Context,
    attrs: AttributeSet?
) : FrameLayout(context, attrs), LifecycleObserver {

    private val mViewPager: ViewPager2
    private val mTabs: TabLayout

    private var mLifecycleOwner: LifecycleOwner? = null
    private var mAutoScrollHandler: Handler? = null
    private var mAutoScrollEnabled = false
    private var mAutoScrollDuration = 0L

    init {
        inflate(context, R.layout.view_sliding_banner, this)

        val typedAttrs = context.obtainStyledAttributes(attrs, R.styleable.SlidingBannerView)

        mViewPager = findViewById(R.id.viewpager)
        mTabs = findViewById(R.id.tabs)

        val hideIndicator = typedAttrs.getBoolean(R.styleable.SlidingBannerView_hideIndicator, false)
        if (hideIndicator) hideIndicator()
        else showIndicator()

        typedAttrs.recycle()
    }

    fun initWithSpacing(
        adapter: SlidingBannerAdapter,
        marginPx: Int,
        offscreenPageLimit: Int = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT,
        mediatorCall: (tab: TabLayout.Tab, position: Int) -> Unit = { _, _ -> }
    ) {
        mViewPager.setPageTransformer(MarginPageTransformer(marginPx))
        init(adapter, offscreenPageLimit, mediatorCall)
    }

    fun init(
        adapter: SlidingBannerAdapter,
        offscreenPageLimit: Int = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT,
        mediatorCall: (tab: TabLayout.Tab, position: Int) -> Unit = { _, _ -> }
    ) {
        mViewPager.adapter = adapter
        mViewPager.offscreenPageLimit = offscreenPageLimit

        TabLayoutMediator(mTabs, mViewPager, true, true) { tab, position ->
            mediatorCall(tab, position)
        }.attach()
    }

    fun hideIndicator() {
        mTabs.visibility = View.GONE
    }

    fun showIndicator() {
        mTabs.visibility = View.VISIBLE
    }

    fun <T : SlidingBannerAdapter.Item> submitItems(items: List<T>) {
        (mViewPager.adapter as? SlidingBannerAdapter)?.submitList(items)
    }

    private fun setupHandler() {
        if (mAutoScrollHandler == null) {
            mAutoScrollHandler = Handler(Looper.myLooper() ?: Looper.getMainLooper())
        }
    }

    private fun runDelayed() {
        if (!mAutoScrollEnabled) return

        mAutoScrollHandler?.postDelayed({
            val currentPosition = mViewPager.currentItem
            val itemCount = mViewPager.adapter?.itemCount ?: 0

            if (itemCount > 0) {
                mViewPager.currentItem = (currentPosition + 1) % itemCount
            }

            if (mAutoScrollEnabled) runDelayed()
        }, mAutoScrollDuration)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() {
        if (!mAutoScrollEnabled) return
        setupHandler()
        runDelayed()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPause() {
        mAutoScrollHandler = null
    }

    fun setAutoScroll(enabled: Boolean = false, ms: Long = 0, lifecycleOwner: LifecycleOwner) {
        mLifecycleOwner = lifecycleOwner
        mAutoScrollEnabled = enabled
        autoScrollDuration(ms)

        mLifecycleOwner?.lifecycle?.addObserver(this)
        if (enabled) runDelayed()
    }

    fun autoScrollDuration(ms: Long) {
        mAutoScrollDuration = ms
    }

}