package com.virgotest.tmdb.utils.zeecommon.util.event

class ActionEvent<out T>(private val content: T) {

    var hasBeenUsed = false
        private set

    fun getContentIfNotUsed(): T? {
        return if (hasBeenUsed) null else {
            hasBeenUsed = true
            content
        }
    }

    fun peekContent(): T = content

}