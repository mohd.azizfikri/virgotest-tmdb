package com.virgotest.tmdb.utils.zeecommon.ui

interface HasBackPressedConfirmation {

    val backPressDelay: Long

}