package com.virgotest.tmdb.utils.zeecommon.ui

interface HasObservers {

    fun setupObservers()

}