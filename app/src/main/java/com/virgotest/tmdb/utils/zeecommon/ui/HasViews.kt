package com.virgotest.tmdb.utils.zeecommon.ui

interface HasViews {

    fun setupViews()

}