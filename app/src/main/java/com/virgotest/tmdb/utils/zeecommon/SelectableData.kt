package com.virgotest.tmdb.utils.zeecommon

data class SelectableData<T>(
    var selected: Boolean = false,
    val origin: T
)