package com.virgotest.tmdb.utils.zeedata.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Configuration(
    @PrimaryKey val identifier: String = "",
    val content: String = ""
) {

    companion object {
        // any configuration keys
        const val KEY_SESSION: String = "USER_SESSION"

        const val YES = "YES"
        const val NO = "NO"
    }

}