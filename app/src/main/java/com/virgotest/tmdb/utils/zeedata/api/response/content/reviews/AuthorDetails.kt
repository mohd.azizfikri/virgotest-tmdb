package com.virgotest.tmdb.utils.zeedata.api.response.content.reviews

data class AuthorDetails(
    val avatar_path: String,
    val name: String,
    val rating: Double,
    val username: String
)