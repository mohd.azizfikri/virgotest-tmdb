package com.virgotest.tmdb.utils.zeedata.api.response.auth

data class TokenResponse(
    val expires_at: String,
    val request_token: String,
    val success: Boolean
)