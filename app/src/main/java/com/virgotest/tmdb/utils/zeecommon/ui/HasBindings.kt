package com.virgotest.tmdb.utils.zeecommon.ui

interface HasBindings {

    fun setupBinding()

}