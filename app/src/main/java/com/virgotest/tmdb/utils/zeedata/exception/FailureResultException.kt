package com.virgotest.tmdb.utils.zeedata.exception

class FailureResultException(message: String) : Exception(message)