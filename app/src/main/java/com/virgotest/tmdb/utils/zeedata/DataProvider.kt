package com.virgotest.tmdb.utils.zeedata

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.virgotest.tmdb.utils.zeedata.api.ApiService
import com.virgotest.tmdb.utils.zeedata.api.interceptor.HeaderTokenInterceptor
import com.virgotest.tmdb.utils.zeedata.db.AppDatabase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object DataProvider {

    private lateinit var mApplicationContext: Application
    private lateinit var mDataConfiguration: DataConfiguration

    private lateinit var mApiService: ApiService
    private lateinit var mAppDatabase: AppDatabase

    private val mDefaultPreferences by lazy {
        getContext().getSharedPreferences(
            getConfig().prefsName,
            Context.MODE_PRIVATE
        )
    }

    @Synchronized
    fun init(app: Application, dataConfiguration: DataConfiguration = DataConfiguration()) {
        mApplicationContext = app
        mDataConfiguration = dataConfiguration

        mAppDatabase = provideAppDatabase(app, dataConfiguration)
        mApiService = provideRetrofit(dataConfiguration, mAppDatabase).create(ApiService::class.java)
    }

    fun getContext(): Context {
        return mApplicationContext
    }

    fun getConfig(): DataConfiguration {
        return mDataConfiguration
    }

    fun getPrefs(): SharedPreferences {
        return mDefaultPreferences
    }

    fun getApiService(): ApiService {
        return mApiService
    }

    fun getAppDatabase(): AppDatabase {
        return mAppDatabase
    }

    /**
     * Retrofit provider
     */
    private fun provideRetrofit(config: DataConfiguration, db: AppDatabase): Retrofit {
        return Retrofit
            .Builder()
            .baseUrl(config.remoteHost)
            .addConverterFactory(GsonConverterFactory.create())
            .client(provideRetrofitClient(config, db))
            .build()
    }

    private fun provideRetrofitClient(config: DataConfiguration, db: AppDatabase): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
//            .addInterceptor(provideHeaderTokenInterceptor(db))
            .addInterceptor(provideHttpLoggingInterceptor(config))
            .writeTimeout(config.remoteTimeout, TimeUnit.SECONDS)
            .readTimeout(config.remoteTimeout, TimeUnit.SECONDS)
            .build()
    }

    private fun provideHttpLoggingInterceptor(config: DataConfiguration): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = if (config.debug) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        }
    }

    private fun provideHeaderTokenInterceptor(db: AppDatabase): HeaderTokenInterceptor {
        return HeaderTokenInterceptor(db)
    }

    /**
     * Room provider
     */
    private fun provideAppDatabase(context: Context, config: DataConfiguration): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, config.databaseName)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

}