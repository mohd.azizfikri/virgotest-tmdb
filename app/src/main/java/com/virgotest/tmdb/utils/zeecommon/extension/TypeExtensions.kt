package com.virgotest.tmdb.utils.zeecommon.extension

import android.annotation.SuppressLint
import android.content.res.Resources
import android.util.DisplayMetrics
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

inline fun <T> T.applyIf(predicate: Boolean, block: T.() -> Unit): T {
    apply { if (predicate) block() }
    return this
}

fun Int?.orNol(): Int = this ?: 0
fun Long?.orNol(): Long = this ?: 0L
fun Double?.orNol(): Double = this ?: 0.0
fun Float?.orNol(): Float = this ?: 0F

fun Number.toReadableFormat(): String {
    val decimalFormat = DecimalFormat().apply {
        decimalFormatSymbols = DecimalFormatSymbols.getInstance().apply {
            groupingSeparator = '.'
            decimalSeparator = ','
        }
    }
    return decimalFormat.format(this)
}

fun Number.toMoneyFormat(prefix: String): String {
    return prefix + toReadableFormat()
}

@SuppressLint("SimpleDateFormat")
fun String.defaultDateFormat(
    output: String = "dd MMM yyyy HH:mm",
    input: String = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
): String {
    val inSdf = SimpleDateFormat(input)
    val outSdf = SimpleDateFormat(output)
    val date = inSdf.parse(this)
    return outSdf.format(date!!)
}

@SuppressLint("SimpleDateFormat")
fun Date.defaultDateFormat(output: String = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"): String {
    val outSdf = SimpleDateFormat(output)
    return outSdf.format(this)
}

/**
 * Convert a Dp to Px
 */
fun Float.asPx(): Float {
    val metrics = Resources.getSystem().displayMetrics
    return this * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

/**
 * Convert a Px to Dp
 */
fun Float.asDp(): Float {
    val metrics = Resources.getSystem().displayMetrics
    return this / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}