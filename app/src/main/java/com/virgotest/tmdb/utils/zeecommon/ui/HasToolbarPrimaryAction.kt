package com.virgotest.tmdb.utils.zeecommon.ui

import android.view.View

interface HasToolbarPrimaryAction {

    val onToolbarPrimaryAction: (v: View) -> Unit

}