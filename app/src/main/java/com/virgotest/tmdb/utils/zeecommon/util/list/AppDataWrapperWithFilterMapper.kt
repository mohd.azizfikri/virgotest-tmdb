package com.virgotest.tmdb.utils.zeecommon.util.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.natpryce.Result
import com.natpryce.onFailure
import com.virgotest.tmdb.utils.zeecommon.util.CombinedTransformation

class AppDataWrapperWithFilterMapper<F, T, E>(
    private val mapper: (source: F) -> T,
    private val blockFilter: (filterEntity: E?, dataList: List<T>) -> List<T>
) {

    val filter by lazy { MutableLiveData<E>() }

    val sourceLiveData by lazy { MutableLiveData(listOf<T>()) }
    val liveData by lazy {
        CombinedTransformation(filter, sourceLiveData) {
            val filterEntity = (it[0] as? E)
            val sourceList = it[1] as? List<T>
            return@CombinedTransformation blockFilter(filterEntity, sourceList.orEmpty())
        }
    }

    val isEmpty by lazy { Transformations.map(liveData) { it.isEmpty() } }
    val isLoading by lazy { MutableLiveData(false) }

    fun setFilter(filterEntity: E) {
        filter.value = filterEntity
    }

    suspend fun load(enableLoading: Boolean = true, block: suspend () -> Result<List<F>, Exception>) {
        if (enableLoading) isLoading.value = true
        sourceLiveData.value = block().onFailure {
            if (enableLoading) isLoading.value = false
            return
        }.map(mapper)
        if (enableLoading) isLoading.value = false
    }
}