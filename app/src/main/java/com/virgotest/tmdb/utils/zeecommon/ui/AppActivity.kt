package com.virgotest.tmdb.utils.zeecommon.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.virgotest.tmdb.R
import com.virgotest.tmdb.utils.zeecommon.CommonProvider
import com.virgotest.tmdb.utils.zeecommon.extension.applyIf
import com.virgotest.tmdb.utils.zeecommon.extension.toast

abstract class AppActivity<out T : ViewDataBinding, out V : AppViewModel>(resource: Int) :
    AppCompatActivity() {

    protected abstract val viewModel: V

    protected val viewBinding: T by lazy { DataBindingUtil.setContentView(this, resource) }

    protected open var confirmationPositiveButtonText: String = "OK"
    protected open var confirmationNegativeButtonText: String = "Cancel"

    private var mCanExit: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding.lifecycleOwner = this // to initialize the viewBinding lazy delegate

        // bindings
        if (this is HasViewModel) viewBinding.setVariable(brViewModelId, viewModel)
        else { CommonProvider.getViewModelBindingId()?.let { id -> viewBinding.setVariable(id, viewModel) } }

        if (this is HasBindings) setupBinding()
        if (this is HasViewModel || this is HasBindings) viewBinding.executePendingBindings()

        // setup / preparation related
        if (this is HasViews) setupViews()
        if (this is HasObservers) setupObservers()

        setupDefaultObserver()
    }

    override fun onBackPressed() {
        if (this is HasBackPressedConfirmation && !mCanExit) {
            toast(getString(R.string.press_back_again_to_exit))

            mCanExit = true
            Handler(Looper.getMainLooper()).postDelayed({
                mCanExit = false
            }, backPressDelay)

            return
        }
        super.onBackPressed()
    }

    private fun setupDefaultObserver() {
        viewModel.showToastMessageEvent.observe(this, ::toast)
        viewModel.showConfirmationMessageEvent.observe(this) {
            showAlertDialog(it.first, true, it.second)
        }
        viewModel.showAlertMessageEvent.observe(this) {
            showAlertDialog(it.first, false, it.second)
        }
    }

    private fun showAlertDialog(
        message: String,
        cancelable: Boolean = true,
        onConfirm: () -> Unit
    ) {
        AlertDialog
            .Builder(this)
            .setCancelable(false)
            .setMessage(message)
            .setPositiveButton(confirmationPositiveButtonText) { dialog, _ ->
                onConfirm()
                dialog.dismiss()
            }
            .applyIf(cancelable) {
                setNegativeButton(confirmationNegativeButtonText) { dialog, _ -> dialog.cancel() }
            }
            .show()
    }

}