package com.virgotest.tmdb.utils.zeewidget.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

abstract class SlidingImageAdapter() : SlidingBannerAdapter() {

    protected open val mLayoutParams by lazy {
        ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    abstract fun onLoadImage(view: ImageView?, item: Item)

    override fun onCreateView(parent: ViewGroup, viewType: Int): View {
        return ImageView(parent.context).apply {
            layoutParams = mLayoutParams
            scaleType = ImageView.ScaleType.CENTER_CROP
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        onLoadImage(holder.itemView as? ImageView, items[position])
    }

}