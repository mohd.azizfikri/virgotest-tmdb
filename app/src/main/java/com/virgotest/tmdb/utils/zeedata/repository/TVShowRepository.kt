package com.virgotest.tmdb.utils.zeedata.repository

import com.natpryce.Result
import com.natpryce.Success
import com.natpryce.onFailure
import com.virgotest.tmdb.utils.zeedata.api.response.content.ContentResponse
import com.virgotest.tmdb.utils.zeedata.api.response.content.reviews.ReviewResponse
import com.virgotest.tmdb.utils.zeedata.util.AppRepository

class TVShowRepository: AppRepository() {

    suspend fun getBanner(): Result<List<ContentResponse>, Exception> {
        val result = responAsResultList { getApi().getTrending(
            media_type = "tv"
        ) }.onFailure { return it }
        val fiveList =  result.subList(0,4)
        return Success(fiveList)
    }

    suspend fun getTrending(): Result<List<ContentResponse>, Exception> {
        val result = responAsResultList { getApi().getTrending(
            media_type = "tv"
        ) }.onFailure { return it }
        return Success(result)
    }

    suspend fun getDiscover(
        page: String = "1",
        language: String = "en-US",
        region: String = "",
        sort_by: String = "popularity.desc",
        certification_country: String = "",
        certification: String = "",
        certificationLte: String = "",
        certificationGte: String = "",
        include_adult: Boolean = false,
        include_video: Boolean = false,
        year: Int? = null
    ): Result<List<ContentResponse>, Exception> {
        val result = responAsResultList { getApi().getDiscoverTV(
            page = page,
            language = language,
            region = region,
            sort_by = sort_by,
            certification_country = certification_country,
            certification = certification,
            certificationLte = certificationLte,
            certificationGte = certificationGte,
            include_adult = include_adult,
            include_video = include_video,
            year = year) }.onFailure { return it }
        return Success(result)
    }

    suspend fun getDetail(contentId: String): Result<ContentResponse, Exception> {
        val result = asDirectResult { getApi().getDetailContent(
            content_id = contentId,
            media_type = "tv"
        ) }.onFailure { return it }
        return Success(result)
    }

    suspend fun getReviews(contentId: String): Result<List<ReviewResponse>, Exception> {
        val result = responAsResultList { getApi().getContentReview(
            content_id = contentId,
            media_type = "tv"
        ) }.onFailure { return it }
        return Success(result)
    }
}