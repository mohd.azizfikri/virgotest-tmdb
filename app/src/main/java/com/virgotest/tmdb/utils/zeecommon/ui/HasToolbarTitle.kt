package com.virgotest.tmdb.utils.zeecommon.ui

interface HasToolbarTitle {

    val toolbarTitle: String

}