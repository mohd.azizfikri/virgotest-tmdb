package com.virgotest.tmdb.utils.zeedata.exception

class IllegalRemoteResultException(message: String) : Exception(message)