package com.virgotest.tmdb.utils.zeedata.util

import com.google.gson.*
import java.lang.reflect.Type;

internal class BooleanTypeAdapter : JsonDeserializer<Boolean> {

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Boolean {
        if ((json as JsonPrimitive).isBoolean) {
            return json.getAsBoolean()
        }

        if (json.isString) {
            val jsonValue = json.getAsString()
            return when {
                jsonValue.equals("true", ignoreCase = true) -> true
                jsonValue.equals("false", ignoreCase = true) -> false
                jsonValue.equals("1", ignoreCase = true) -> true
                jsonValue.equals("0", ignoreCase = true) -> false
                else -> false
            }
        }

        val code = json.getAsInt()
        return code == 1
    }
}
