package com.virgotest.tmdb.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ServiceComponent
import com.virgotest.tmdb.utils.zeedata.repository.*

@Module
@InstallIn(ActivityComponent::class, ServiceComponent::class)
object RepositoryModule {

    @Provides
    fun provideCommonRepository() = CommonRepository()

    @Provides
    fun provideMovieRepository() = MovieRepository()

    @Provides
    fun provideTVShowRepository() = TVShowRepository()

}