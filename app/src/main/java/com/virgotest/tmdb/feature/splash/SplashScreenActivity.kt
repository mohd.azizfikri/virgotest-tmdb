package com.virgotest.tmdb.feature.splash

import android.os.CountDownTimer
import android.util.Log
import androidx.activity.viewModels
import com.virgotest.tmdb.R
import com.virgotest.tmdb.databinding.ActivitySplashScreenBinding
import com.virgotest.tmdb.feature.main.MainActivity
import com.virgotest.tmdb.utils.zeecommon.extension.toast
import com.virgotest.tmdb.utils.zeecommon.ui.AppActivity
import com.virgotest.tmdb.utils.zeecommon.ui.HasObservers
import com.virgotest.tmdb.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreenActivity : AppActivity<ActivitySplashScreenBinding, SplashScreenViewModel>
    (R.layout.activity_splash_screen), HasObservers {
    override val viewModel by viewModels <SplashScreenViewModel>()

    override fun setupObservers() {
        viewModel.session.observe(this){
            if(!it.isNullOrEmpty()){
                enterDashboard()
            }
        }
    }

    private fun enterDashboard(){
        object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.w("Seconds remaining",(millisUntilFinished / 1000).toString())
            }

            override fun onFinish() {
                MainActivity.launch(this@SplashScreenActivity, true)
            }
        }.start()
    }
}