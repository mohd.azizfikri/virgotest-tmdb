package com.virgotest.tmdb.feature.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.natpryce.Success
import com.natpryce.onFailure
import com.virgotest.tmdb.common.adapter.ReviewAdapter
import com.virgotest.tmdb.utils.zeecommon.SelectableData
import com.virgotest.tmdb.utils.zeecommon.ui.AppViewModel
import com.virgotest.tmdb.utils.zeecommon.util.list.AppDataWrapper
import com.virgotest.tmdb.utils.zeedata.api.response.content.ContentResponse
import com.virgotest.tmdb.utils.zeedata.api.response.content.reviews.ReviewResponse
import com.virgotest.tmdb.utils.zeedata.repository.MovieRepository
import com.virgotest.tmdb.utils.zeedata.repository.TVShowRepository
import kotlinx.coroutines.launch

class ContentDetailViewModel @ViewModelInject constructor(
    private val mMovieRepository: MovieRepository,
    private val mTVShowRepository: TVShowRepository
) : AppViewModel(), ReviewAdapter.ContentListener {

    val mDetailContent by lazy {MutableLiveData<ContentResponse>()}

    val mIsMovie by lazy {MutableLiveData(true)}
    private val mIdContent by lazy { MutableLiveData<String>() }

    val reviewListWrapper by lazy { AppDataWrapper<SelectableData<ReviewResponse>>() }

    fun setup(isMovie: Boolean, idContent: String){
        mIsMovie.value = isMovie
        mIdContent.value = idContent
        fetchDetail()
        fetchReview()
    }

    fun fetchDetail(){
        viewModelScope.launch {
            mDetailContent.value = when(mIsMovie.value) {
                false -> {
                    mTVShowRepository.getDetail(mIdContent.value.orEmpty()).onFailure {
                        return@launch
                    }
                }
                else -> {
                    mMovieRepository.getDetail(mIdContent.value.orEmpty()).onFailure {
                        return@launch
                    }
                }
            }
        }
    }

    fun fetchReview() {
        viewModelScope.launch {
            reviewListWrapper.load {
                val results =
                    when (mIsMovie.value) {
                        false -> {
                            mTVShowRepository.getReviews(mIdContent.value.orEmpty()).onFailure {
                                return@load Success(emptyList())
                            }
                        }
                        else -> {
                            mMovieRepository.getReviews(mIdContent.value.orEmpty()).onFailure {
                                return@load Success(emptyList())
                            }
                        }

                    }
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClickItem(data: ReviewResponse) {
        //TODO("Not yet implemented")
    }
}