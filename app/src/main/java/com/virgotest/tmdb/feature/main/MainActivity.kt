package com.virgotest.tmdb.feature.main

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.virgotest.tmdb.R
import com.virgotest.tmdb.databinding.ActivityMainBinding
import com.virgotest.tmdb.feature.main.page.home.HomeFragment
import com.virgotest.tmdb.feature.main.page.account.AccountFragment
import com.virgotest.tmdb.utils.zeecommon.extension.startActivity
import com.virgotest.tmdb.utils.zeecommon.extension.startActivityAsNewTask
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val mStartupPage = intent?.getSerializableExtra(EXTRA_STARTUP) as? Page

    private val mPages  =
        mapOf(
            Page.HOME to { HomeFragment() },
            Page.ACCOUNT to { AccountFragment() }
        )

    private fun showPage(page: Page): Boolean {
        mPages[page]?.let { fragment ->
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl_main, fragment() as Fragment)
                .commit()
            return true
        }
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        showPage(Page.HOME)

        mStartupPage?.let {
            when(it){
                Page.HOME -> showPage(Page.HOME)
                Page.ACCOUNT -> showPage(Page.ACCOUNT)
            }
        }

        binding.bnvMain.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> showPage(Page.HOME)
                R.id.navigation_account -> showPage(Page.ACCOUNT)
                else -> showPage(Page.HOME)
            }
        }
    }

    companion object {
        private const val EXTRA_STARTUP = "EXTRA_STARTUP"
        enum class Page { HOME, ACCOUNT }
        fun launch(activity: Activity, asNewTask: Boolean = false, startup: Page = Page.HOME) {
            if (asNewTask) activity.startActivityAsNewTask<MainActivity>() {
                putExtra(
                    EXTRA_STARTUP,
                    startup
                )
            }
            else activity.startActivity<MainActivity> { putExtra(EXTRA_STARTUP, startup) }
        }
    }
}