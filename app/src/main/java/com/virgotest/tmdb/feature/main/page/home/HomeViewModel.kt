package com.virgotest.tmdb.feature.main.page.home

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.natpryce.Success
import com.natpryce.onFailure
import com.virgotest.tmdb.common.adapter.ContentAdapter
import com.virgotest.tmdb.common.adapter.HomeBannerAdapter
import com.virgotest.tmdb.common.data.Banner
import com.virgotest.tmdb.common.data.Item
import com.virgotest.tmdb.utils.zeecommon.SelectableData
import com.virgotest.tmdb.utils.zeecommon.ui.AppViewModel
import com.virgotest.tmdb.utils.zeecommon.util.event.LiveEvent
import com.virgotest.tmdb.utils.zeecommon.util.event.MutableLiveEvent
import com.virgotest.tmdb.utils.zeecommon.util.list.AppDataWrapper
import com.virgotest.tmdb.utils.zeecommon.util.list.AppDataWrapperWithMapper
import com.virgotest.tmdb.utils.zeedata.api.response.content.ContentResponse
import com.virgotest.tmdb.utils.zeedata.repository.MovieRepository
import com.virgotest.tmdb.utils.zeedata.repository.TVShowRepository
import kotlinx.coroutines.launch

class HomeViewModel  @ViewModelInject constructor(
    private val mMovieRepository: MovieRepository,
    private val mTVShowRepository: TVShowRepository
) : AppViewModel(),
    HomeBannerAdapter.HomeBannerListener,
    ContentAdapter.ContentListener{

    private val _onOpenDetailPageEvent by lazy { MutableLiveEvent<Item>() }
    val onOpenDetailPageEvent: LiveEvent<Item> = _onOpenDetailPageEvent

    val bannerList by lazy { AppDataWrapperWithMapper(Banner::map) }
    val trendingListWrapper by lazy { AppDataWrapper<SelectableData<ContentResponse>>() }
    val discoverListWrapper by lazy { AppDataWrapper<SelectableData<ContentResponse>>() }

    val isMovie by lazy {MutableLiveData(true)}


    init {
        setup()
    }

    fun setup(){
        fetchBanner(isMovie.value)
        fetchTrending(isMovie.value)
        fetchDiscover(isMovie.value)
    }

    fun changeContent(mIsMovie: Boolean){
        isMovie.value = mIsMovie
        setup()
    }

    fun fetchBanner(isMovie: Boolean? = true){
        viewModelScope.launch {
            bannerList.load {
                val result = when(isMovie) {
                    false -> {
                        mTVShowRepository.getBanner().onFailure {
                            return@load Success(emptyList())
                        }
                    }
                    else -> {
                        mMovieRepository.getBanner().onFailure {
                            return@load Success(emptyList())
                        }
                    }
                }

                Success(result)
            }
        }
    }

    fun fetchTrending(isMovie: Boolean? = true){
        viewModelScope.launch {
            trendingListWrapper.load {
                val results =
                    when(isMovie) {
                        false -> {
                            mTVShowRepository.getTrending().onFailure {
                                return@load Success(emptyList())
                            }
                        }
                        else -> {
                            mMovieRepository.getTrending().onFailure {
                                return@load Success(emptyList())
                            }
                        }
                    }
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    fun fetchDiscover(isMovie: Boolean? = true){
        viewModelScope.launch {
            discoverListWrapper.load {
                val results =
                    when(isMovie) {
                        false -> {
                            mTVShowRepository.getDiscover().onFailure {
                                return@load Success(emptyList())
                            }
                        }
                        else -> {
                            mMovieRepository.getDiscover().onFailure {
                                return@load Success(emptyList())
                            }
                        }
                    }
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClick(item: Banner) {
        //TODO("Nothing To do here")
    }

    override fun onClickItem(data: ContentResponse) {
        _onOpenDetailPageEvent.set(Item(data.id.toString(), isMovie.value.toString()))
    }

}