package com.virgotest.tmdb.feature.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.natpryce.onFailure
import com.virgotest.tmdb.utils.zeecommon.ui.AppViewModel
import com.virgotest.tmdb.utils.zeedata.repository.CommonRepository
import kotlinx.coroutines.launch

class SplashScreenViewModel @ViewModelInject constructor(
        private val mCommonRepository: CommonRepository
): AppViewModel() {

    val session by lazy { MutableLiveData("") }

    init {
        checkSession()
    }

    fun checkSession(){
        viewModelScope.launch {
            val result = mCommonRepository.getSavedSessionID().onFailure {
                createSession()
                return@launch
            }
            session.value = result
        }
    }

    fun createSession(){
        viewModelScope.launch {
            val result = mCommonRepository.createGuestSession().onFailure {
                    _showToastMessageEvent.set(it.reason.toString())
                    return@launch
                }
            session.value = result.guest_session_id
        }
    }
}