package com.virgotest.tmdb.feature.detail

import android.app.Activity
import androidx.activity.viewModels
import com.virgotest.tmdb.R
import com.virgotest.tmdb.common.adapter.HomeBannerAdapter
import com.virgotest.tmdb.common.adapter.ReviewAdapter
import com.virgotest.tmdb.databinding.ActivityContentDetailBinding
import com.virgotest.tmdb.utils.zeecommon.extension.startActivity
import com.virgotest.tmdb.utils.zeecommon.ui.AppActivity
import com.virgotest.tmdb.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContentDetailActivity : AppActivity<ActivityContentDetailBinding, ContentDetailViewModel>
    (R.layout.activity_content_detail), HasViews {

    override val viewModel by viewModels<ContentDetailViewModel>()

    private val isMovie by lazy { intent.getBooleanExtra(IS_MOVIE, true) }
    private val id by lazy { intent.getStringExtra(CONTENT_ID) }

    private val mReviewAdapter by lazy { ReviewAdapter(viewModel) }

    override fun setupViews() {
        viewModel.setup(isMovie, id.orEmpty())

        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }

        viewBinding.rvReview.adapter = mReviewAdapter
    }

    companion object {
        private const val CONTENT_ID = "CONTENT_ID"
        private const val IS_MOVIE = "IS_MOVIE"
        fun launch(activity: Activity, isMovie: Boolean, id_content: String) {
            activity.startActivity<ContentDetailActivity> {
                putExtra(CONTENT_ID, id_content)
                putExtra(IS_MOVIE, isMovie)
            }
        }
    }
}