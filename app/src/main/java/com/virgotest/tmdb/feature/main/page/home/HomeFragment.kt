package com.virgotest.tmdb.feature.main.page.home

import androidx.browser.trusted.Token.deserialize
import androidx.fragment.app.viewModels
import com.virgotest.tmdb.R
import com.virgotest.tmdb.common.adapter.ContentAdapter
import com.virgotest.tmdb.common.adapter.HomeBannerAdapter
import com.virgotest.tmdb.databinding.FragmentHomeBinding
import com.virgotest.tmdb.feature.detail.ContentDetailActivity
import com.virgotest.tmdb.utils.zeecommon.ui.AppFragment
import com.virgotest.tmdb.utils.zeecommon.ui.HasObservers
import com.virgotest.tmdb.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : AppFragment<FragmentHomeBinding, HomeViewModel>
  (R.layout.fragment_home), HasViews, HasObservers{

  override val viewModel by viewModels<HomeViewModel>()

  private val mBannerAdapter by lazy { HomeBannerAdapter(viewModel) }
  private val mContentAdapter by lazy { ContentAdapter(viewModel) }

  override fun setupViews() {
    //Set up banner
    viewBinding.slidingBanner.init(mBannerAdapter)
    viewBinding.slidingBanner.setAutoScroll(true, 5000, viewLifecycleOwner)

    //Set up Trending
    viewBinding.rvTrending.adapter = mContentAdapter

    //Set up Discover
    viewBinding.rvDiscover.adapter = mContentAdapter
  }


  override fun setupObservers() {
    viewModel.bannerList.liveData.observe(viewLifecycleOwner) {
      viewBinding.slidingBanner.submitItems(it)
    }

    viewModel.onOpenDetailPageEvent.observe(this) {
      ContentDetailActivity.launch(requireActivity(), it.name == "true", it.id)
    }
  }

}