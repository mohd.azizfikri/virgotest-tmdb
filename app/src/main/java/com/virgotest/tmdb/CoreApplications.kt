package com.virgotest.tmdb

import android.app.Application
import com.virgotest.tmdb.utils.Util
import com.virgotest.tmdb.utils.zeecommon.CommonProvider
import com.virgotest.tmdb.utils.zeedata.DataConfiguration
import com.virgotest.tmdb.utils.zeedata.DataProvider
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoreApplications: Application() {

    override fun onCreate() {
        super.onCreate()

        Util.init(this)
        CommonProvider.init(BR.viewmodel)
        DataProvider.init(
            this, DataConfiguration(
                remoteHost = "https://api.themoviedb.org/",
                remoteAPIkey = "9ae74191c0b6db7d9d92dd33c3594241",
                remoteTimeout = 60L
            )
        )
    }
}