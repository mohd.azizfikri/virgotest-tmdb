package com.virgotest.tmdb.common.adapter

import android.widget.ImageView
import coil.load
import com.virgotest.tmdb.R
import com.virgotest.tmdb.common.data.Banner
import com.virgotest.tmdb.utils.zeewidget.adapter.SlidingImageAdapter

class HomeBannerAdapter(private val mHomeBannerListener: HomeBannerListener) :
    SlidingImageAdapter() {

    override fun onLoadImage(view: ImageView?, item: Item) {
        if (item is Banner) {
            view?.load(item.imgUrl) {
                placeholder(R.drawable.img_placeholder_banner)
                fallback(R.drawable.img_placeholder_banner)
            }
            view?.setOnClickListener {
                mHomeBannerListener.onClick(item)
            }
        }
    }

    interface HomeBannerListener {
        fun onClick(item: Banner)
    }

}