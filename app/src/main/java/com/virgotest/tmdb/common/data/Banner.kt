package com.virgotest.tmdb.common.data

import com.virgotest.tmdb.utils.zeedata.api.response.content.ContentResponse
import com.virgotest.tmdb.utils.zeewidget.adapter.SlidingBannerAdapter

class Banner(
    val id: String,
    val imgUrl: String,
    val title: String
) : SlidingBannerAdapter.Item(imgUrl) {

    companion object {
        fun map(src: ContentResponse): Banner {
            return Banner(
                src.id.toString(),
                "https://image.tmdb.org/t/p/w500" + src.backdrop_path,
                src.title
            )
        }

    }
}