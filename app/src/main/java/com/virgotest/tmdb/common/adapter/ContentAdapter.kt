package com.virgotest.tmdb.common.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.virgotest.tmdb.databinding.ItemContentBinding
import com.virgotest.tmdb.utils.zeecommon.SelectableData
import com.virgotest.tmdb.utils.zeecommon.util.list.AppRecyclerView
import com.virgotest.tmdb.utils.zeedata.api.response.content.ContentResponse

class ContentAdapter(
    private val mListener: ContentListener
) : AppRecyclerView<ItemContentBinding, SelectableData<ContentResponse>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemContentBinding = ItemContentBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemContentBinding,
        model: SelectableData<ContentResponse>
    ) {
        binding.data = model.origin

        binding.baseLayout.setOnClickListener {
            mListener.onClickItem(model.origin)
        }
    }

    interface ContentListener {
        fun onClickItem(data: ContentResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<ContentResponse>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<ContentResponse>,
                newItem: SelectableData<ContentResponse>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<ContentResponse>,
                newItem: SelectableData<ContentResponse>
            ): Boolean {
                return oldItem.origin.id == newItem.origin.id
            }

        }
    }
}