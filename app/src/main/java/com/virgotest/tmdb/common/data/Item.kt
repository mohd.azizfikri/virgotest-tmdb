package com.virgotest.tmdb.common.data

import androidx.recyclerview.widget.DiffUtil

data class Item(val id: String, val name: String) {

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<Item>() {

            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }

}