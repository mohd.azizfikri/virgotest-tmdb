package com.virgotest.tmdb.common.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.virgotest.tmdb.databinding.ItemContentReviewsBinding
import com.virgotest.tmdb.utils.zeecommon.SelectableData
import com.virgotest.tmdb.utils.zeecommon.util.list.AppRecyclerView
import com.virgotest.tmdb.utils.zeedata.api.response.content.reviews.ReviewResponse

class ReviewAdapter(
    private val mListener: ContentListener
) : AppRecyclerView<ItemContentReviewsBinding, SelectableData<ReviewResponse>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemContentReviewsBinding = ItemContentReviewsBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemContentReviewsBinding,
        model: SelectableData<ReviewResponse>
    ) {
        binding.data = model.origin

        binding.baseLayout.setOnClickListener {
            mListener.onClickItem(model.origin)
        }
    }

    interface ContentListener {
        fun onClickItem(data: ReviewResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<ReviewResponse>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<ReviewResponse>,
                newItem: SelectableData<ReviewResponse>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<ReviewResponse>,
                newItem: SelectableData<ReviewResponse>
            ): Boolean {
                return oldItem.origin.id == newItem.origin.id
            }

        }
    }
}